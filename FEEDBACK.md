I realize some of this feedback may be a bit ranty. Please don't take it
personally!

- Why the global installs? Just use npx + npm scripts. Pollute people's
  environments as little as possible.
- Take advantage of the fact that `/path/to/someModule` resolves to
  `/path/to/someModule/index.js` in CommonJS. This saves the visual clutter of
  duplication in paths.
- In fact, the whole code base is kind of cluttered. There are extraneous
  comments (e.g. "Require our modules" right before you literally require
  modules — all that does is add noise, which takes away clarity instead of
  providing it) and unused variables all over the place. In a "normal" code base
  this would be annoying but sometimes it's part of the job — for a test
  assignment, please just make sure things like this aren't an issue.
- The URLs used are not exactly RESTful. Not even just the one(s) prefixed with
  `/apiv1/` — I'm not even sure why you put that in there in this case. For
  example, instead of a `/users/myprofile` path that gives different output
  depending on which user is logged in, I would expect to have a URL like
  `/users/:id/profile` that returns profile data for the user corresponding to a
  specific `id`, and if you are not logged in as that user (or an admin if
  admins have rights to view other people's profiles), a 403
  is returned. Also, I would have chosen to separate RPC-like functionality like
  cache clearing into a clearly and explicitly separated route, like `/rpc/`,
  `/actions/`, or `/maintenance/`.
- Maybe this is just a semantic thing but I think in programming semantics are
  important so I just want to say it — this was not a "gateway" until I added
  the jokes route. Everything else is just an API, a gateway is where you take
  data from one place and route it through another. I would recommend changing
  the language around that in the assignment PDF.