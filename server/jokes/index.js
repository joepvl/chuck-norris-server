const router = require("express").Router();
const request = require("request-promise-native");
const logger = require("../utils/logger");
const cacheController = require("../caching/controller/caching-controller");

const jokesPassthru = path => {
  // Since the requirements don't specify support for other query string params,
  // we can just append `?escape=javascript` here to avoid having to unescape or
  // use `dangerouslySetInnerHTML` in the client app.
  const options = {
    uri: `http://api.icndb.com/jokes${path}?escape=javascript`,
    json: true
  };

  return request(options);
};

const sendGatewayError = res => error => {
  res.status(500).send(`Gateway error: ${error.message}`);
};

router.get("/:id(\\d+)", (req, res) => {
  // Cache individual jokes.
  cacheController
    .getMemoryCache()
    .wrap(`joke-${req.params.id}`, () => {
      logger.info(`Joke ${req.params.id} not found in cache, fetching...`);
      return jokesPassthru(req.url);
    })
    .then(jokes => res.send(jokes))
    .catch(sendGatewayError(res));
});

router.get("/random/amount", (req, res) => {
  jokesPassthru(req.url)
    .then(jokes => res.send(jokes))
    .catch(sendGatewayError(res));
});

router.get("*", (req, res) => {
  jokesPassthru(req.url)
    .then(jokes => res.send(jokes))
    .catch(sendGatewayError(res));
});

module.exports = router;
